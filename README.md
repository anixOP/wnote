# WNote

WNote - это веб-приложение для создания и управления заметками. Пользователи могут создавать, редактировать и удалять заметки, организовывать их по категориям и иметь к ним доступ из любого устройства, подключенного к интернету.

## Функциональность
- Создание новых заметок указанной категории
- Редактирование существующих заметок
- Удаление заметок
- Организация заметок по категориям
- Возможность доступа к заметкам из различных устройств с поддержкой интернета

## Установка
- git clone https://gitlab.com/anixOP/wnote
- cd wnote
- npm i
- npm install react-router-dom --save
- npm install react-icons --save
- npm start


## Технологии
WNote написан на JavaScript с использованием Node.js, Express.js, и MongoDB для базы данных.

## Благодарности
WNote разработан благодаря использованию открытых исходных кодов других проектов.

## Лицензия
Этот проект лицензирован в соответствии с условиями лицензии MIT. 

Спасибо, что выбрали WNote!
